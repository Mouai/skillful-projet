@extends('default.main')

@section('content')

<h2>{{ $team->name }}</h2>

crée depuis le {{ date('d M Y', strtotime($team->created_at)) }}<br><br>

Liste des joueurs :<br><br>
@foreach($team->users as $user)
{{ HTML::linkRoute(player,  $user->alias,  $user->id) }}<br>

@endforeach


@stop