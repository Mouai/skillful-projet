<nav class="top-bar" data-topbar="" role="navigation">
    <ul class="title-area">
        <li class="name">
            <h1><a href="{{URL::to  ('home.index',null)}}">{{ HTML::image('img/logo.png') }}</h1></a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href=""><span>Menu</span></a></li>
    </ul>

    <section class="top-bar-section">

        <ul class="left">
            <li><a id="matchmaking" href="{{route('matchmaking.search',null)}}">Start <span class="squared">Matchmaking</span></a></li>
        </ul>

        <ul class="right">
            <li>{{ HTML::linkRoute('home.index', 'Home') }}</li>
            <li>{{ HTML::linkRoute('player.profile', 'Profile', [Session::get('player')->getId()]) }}</li>
            <li>{{ HTML::linkRoute('user.logout', 'Logout') }}</li>
        </ul>

    </section>
</nav>