<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
    <meta charset="utf-8">
    <title>Skillful</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {{ HTML::style('css/test.css') }}
    {{ HTML::style('css/app.css') }}
    {{ HTML::script('js/min/scripts_header.min.js') }}
</head>
<body>
    <div id="root" class="fullheight">

    @if($errors->has())
       @foreach ($errors->all() as $error)
          <div>{{ $error }}</div>
      @endforeach
    @endif


        <div class="container fullheight">
            @if(Session::has('user'))
                @include('default.menu')
            @endif

            @yield('content')
            <!--
            <div id="content">

            </div>


            <div id="matchmaking-pane">
            @include('matchmaking.pane')
            </div>-->


            {{ HTML::script('js/min/scripts_footer.min.js') }}
            <script>
                $(document).ready(function(){
                    $(document).foundation();
                    //$('.chaffle').chaffle({ speed: 30, time: 25 });
                });
            </script>
            @yield('custom_scripts')
        </div>
    </div>
</body>
</html>

