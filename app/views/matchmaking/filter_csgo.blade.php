{{ Form::open() }}

{{ Form::label('server', "J'ai un serveur") }}
{{ Form::checkbox('server') }}

{{ Form::label('map', "Selectionner une map") }}
{{ Form::select('map', $maps) }}


{{ Form::label('level', "Selectionner un niveau") }}
{{ Form::select('level', $levels) }}

{{ Form::button('Lancer la recherche', ['class' => 'start_search']) }}

{{ Form::close() }}