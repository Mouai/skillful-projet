@extends('default.main')

@section('content')


{{ Form::open(['class' => 'select_team']) }}

{{ Form::label('team_id', 'Selectionner une team') }}
{{ Form::select('team_id', $teams) }}


{{ Form::button('GO', ['id' => 'btn_go']) }}
{{ Form::close() }}
<h2>Filtres</h2>

<div id="filters"></div>

 <div class="content-chat">
    <div class= "content-center">
        <div class="content-messages"></div>
        <input type="text" id="msg" />

        <input type="button" id="btn_send" value="Envoyer"/>
    </div>
</div>


@stop

@section('custom_scripts')

<script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>

<script>

jQuery(document).ready(function(){
        var user_id = "{{ Auth::user()->id }}";
        var client = new Client(user_id);
        client.url_ajax_get_filter = "{{ URL('matchmaking/ajax_getFilterByTeam') }}";
        client.initialize();
});

</script>
{{ HTML::script('js/client.class.js') }}
@stop