@extends('default.main')

@section('content')


@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif



{{ Form::open(['route' => 'user.login']) }}

{{ Form::label('email', Lang::get('auth.form-email') ) }}
{{ Form::text('email') }}

{{ Form::label('password', Lang::get('auth.form-password')) }}
{{ Form::password('password') }}


{{ Form::submit('Se connecter') }}
{{ Form::close() }}



@stop

