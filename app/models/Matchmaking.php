<?php

class Matchmaking extends Eloquent
{

    protected $table = 'matchmaking';

    public function game()
    {
        return $this->hasOne('Game');
    }

    public function searcher()
    {
        return $this->hasOne('User', 'searcher_id');
    }

    public function opponent()
    {
        return $this->hasOne('User', 'opponent_id');
    }

} 