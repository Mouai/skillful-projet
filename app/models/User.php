<?php

class User{

    protected $id;
    protected $email;

    public function __construct($id, $email){
        $this->id = $id;
        $this->email = $email;
    }

    /**
     * Récupère les informations de la sessions d'un utilisateur
     */
    public function getSessionData()
    {
        return Session::all();
    }


    public static function rules(){
        return array(
            'email' => 'required|email',
            'password' => 'required'
        );
    }


    /**
     * Getter id
     */
    public function getId(){
        return $this->id;
    }
}
