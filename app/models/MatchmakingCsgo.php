<?php

class MatchmakingCsgo extends Eloquent {

    protected $table = 'matchmaking_csgo';

    public function map()
    {
        return $this->hasOne('MapCsgo');
    }

}