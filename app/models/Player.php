<?php

class Player{

    protected $id;
    //protected $alias;

    public function __construct($id){
        $this->id = $id;
        //$this->$alias = $alias;
    }

    /**
     * Getter id
     */
    public function getId(){
        return $this->id;
    }
}
