<?php
class Team extends Eloquent
{

    protected $table = 'team';

    /**
     * Liste des joueurs de la team
     */
    public function users(){
        return $this->belongsToMany('User');
    }


    /**
     * Le jeu de la team
     */
    public function game(){
        return $this->belongsTo('Game');
    }
}