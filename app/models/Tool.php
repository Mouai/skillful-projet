<?php
class Tool {

    /**
     *
     * @return string
     */
    public function getFormattedDate() {
        $createdDate = new DateTime($this->getCreatedAt());
        $actualDate = new DateTime;
        $diff = $createdDate->diff($actualDate);
        if ($diff->format("%y") >= 1) {
            return $diff->format("y years ago");
        } elseif ($diff->format("%m") >= 1) {
            return $diff->format("%m month ago");
        } elseif ($diff->format("%a") >= 1) {
            return $diff->format("%a days ago");
        } elseif ($diff->format("%h") >= 1) {
            return $diff->format("%h hours ago");
        } elseif ($diff->format("%i") >= 1) {
            return $diff->format("%i mns ago");
        } else {
            return "few seconds ago";
        }
    }
}