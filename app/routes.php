<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::when('*','csrf', ['post','put','delete']);

Route::filter('json', function()
{
    Response::header('Content-Type', 'application/json');
});

Route::group(array('before' => 'guest'), function(){
    /*---------- ROOT ----------*/
    Route::get('/' ,['as' => 'user.showLogin','uses' => 'UserController@showLogin']);

    Route::post('/login' ,['as' => 'user.login','uses' => 'UserController@doLogin']);
});



Route::group(array('before' => 'auth'), function(){

    /*---------- HOME ----------*/
    Route::get('home/index' ,['as' => 'home.index','uses' => 'HomeController@index']);

    /*---------- USER ----------*/
    //Route::get('user/login' ,['as' => 'user.login','uses' => 'UserController@s']);
    Route::get('/logout' ,['as' => 'user.logout','uses' => 'UserController@doLogout']);



    /*---------- PLAYER ----------*/
    Route::get('player/{player_id}' ,['as' => 'player.profile','uses' => 'PlayerController@showProfile']);
    Route::get('player/{player_id}' , function($player_id){
        return View::make('player.profile');
    });

    /*---------- TEAM ----------*/
    Route::resource('team', 'TeamController');
    Route::get('team/{team_id}' ,['as' => 'team.profile','uses' => 'TeamController@showProfile']);

    /*---------- MATCHMAKING ----------*/
    Route::get('matchmaking/search' ,['as' => 'matchmaking.search','uses' => 'MatchmakingController@search']);
    Route::post('matchmaking/ajax_getFilterByTeam' ,['as' => 'matchmaking.getFilterByTeam','uses' => 'MatchmakingController@getFilterByTeam']);
});


Route::get('team/getTeamById/{team_id}', ['as' => 'team.getteambyid','uses' => 'TeamController@getTeamById']);

