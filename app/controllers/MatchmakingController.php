<?php

class MatchmakingController extends BaseController{

    /**
     * Azffiche la page de recherche de matchmaking
     */
    public function search(){
        $teams = User::find(Auth::user()->id)->teams()->lists('name', 'id');

        return View::make('matchmaking.search', compact('teams'));
    }

    /**
     * Récupère le bon formulaire en fonction du jeu de la team
     */
    public function getFilterByTeam(){


        $data = Input::all();
        $game = Team::find(Input::get('team_id'))->game;


        switch($game->tag){
            case "CS:GO":
                $maps = MapCsgo::get()->lists('name', 'id');
                $levels = LevelCsgo::get()->lists('name', 'id');
                return View::make('matchmaking.filter_csgo', compact('maps', 'levels'));
                break;
            case 'LoL':
                $levels = LevelLol::get()->lists('name', 'id');
                return View::make('matchmaking.filter_lol', compact('levels'));
                break;

        }
    }


    /**
     * Ajax recherchant une équipe
     */
    public function ajax_search(){
        //$etat -> etat d'un match - recherche - acceptation - actif - termine


        //On test si on est deja inscrit


        //Sinon on l'inscrit
    }
}