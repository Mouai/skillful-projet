<?php
/**
 * Created by PhpStorm.
 * User: Mouai
 * Date: 20/12/2014
 * Time: 11:58
 */

class TeamController extends BaseController{

    /**
     * Affiche le profil d'une team
     */
    public function show($id){
        $team = Team::find($id);
        $team->users = Team::find($id)->users;
        //dd($team->toArray());

        return View::make('team.profile', compact('team'));
    }

    /**
     * Retourne les informations d'une équipe
     */
    public function getTeamById($id){
        $team = Team::find($id);
        return Response::json($team);

    }
} 