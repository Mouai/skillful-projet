<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class BaseController extends Controller
{


    protected $httpClient;

    public function BaseController()
    {
        $this->httpClient = new Client();
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

}
