<?php
use GuzzleHttp\Client;
use GuzzleHttp\Message\Response;

class PlayerController extends \BaseController
{

    /**
     * Affiche le profil d'un joueur
     */
    public function showProfile($id)
    {

        $url = Config::get('constants.API') . '/player/profile/'.$id;
        //dd($url);

        $response = $this->httpClient->get($url);

        if ($player = $response->json()) {
            //echo $body;
            //dd($player);
        }
        return View::make('player.profile', compact('player'));
    }

}
