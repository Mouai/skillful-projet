<?php

use GuzzleHttp\Message\Response;

class UserController extends \BaseController
{

    /**
     * Affiche le formulaire de connexion
     */
    public function showLogin()
    {
        return View::make('index');
    }

    /**
     * Effectue la connexion d'un joueur
     */
    public function doLogin()
    {
        //dd('UserController@doLogin');

        // TODO : Remove this backdoor when Raspberry PI is set up
        if(App::environment('local') && (Input::get('email') == 'eric.iLife@gmail.com'))
        {
            Session::set('user', new User(1, 'eric.iLife@gmail.com'));
            Session::set('player', new Player('55230412c632bd7c1f1df5f8'));
            return Redirect::route('home.index')->with('message', "Vous êtes connecté !");
        }

        $input = Input::all();
        $validator = Validator::make($input, User::rules());

        if ($validator->fails()) {
            return Redirect::route('user.showLogin')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $userdata = array
            (
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            //$request = Request::create(Config::get('constants.API').'/login', 'POST');
            //$response = Route::dispatch($request);

            $dataSend = ['json' => [
                'email' => Input::get('email'),
                'password' => Input::get('password')
            ]];


            try {
                $request = $this->httpClient->createRequest('POST', Config::get('constants.API') . '/login', $dataSend);

                $response = $this->httpClient->send($request);
                echo $response->getStatusCode() . "\n";
                $body = json_decode($response->getBody());

                if ($body->login) {
                    //dd($body);
                    $user = new User($body->id, $body->email);
                    $player = new Player($body->player_id);

                    Session::set('user', $user);
                    Session::set('player', $player);

                    return Redirect::route('home.index')
                        ->with('message', "Vous êtes connecté !");
                } else {
                    return Redirect::route('user.showLogin')
                        ->withErrors('Echec à la connexion');
                }
            } catch (RequestException $e) {
                dd('pas ok');
                echo $e->getRequest() . "\n";
                if ($e->hasResponse()) {
                    echo $e->getResponse() . "\n";
                }
                return Redirect::route('user.showLogin')
                    ->withErrors('Echec à la connexion');

            }


            /*if(Auth::attempt($userdata))
            {
                Session::push('user.id', Auth::user()->id);
                //test memcached
                Cache::put('un', '1', 2);
                return Redirect::route('home.index')
                    ->with('message', "Vous êtes connecté !");
            }
            else
            {
                return Redirect::route('user.showLogin')
                    ->with('message', 'Echec à la connexion');
            }*/
        }
    }

    /**
     * Effectue l'inscription d'un joueur
     */
    public function doRegister()
    {

    }

    /**
     * Déconnecte le joueur
     */
    public function doLogout()
    {
        Session::forget('user');
        //Session::flush();

        return Redirect::route('user.logout')
            ->with('message', 'Vous êtes deconnecté');
    }

    /**
     * Affiche le profil d'un joueur
     */
    public function showProfile($id)
    {

        $dataSend = ['json' => ['id' => $id]];
        $response = $this->httpClient->get(Config::get('constants.API') . '/user/'.$id);
        if ($body = $response->json()) {
            //echo $body;
            dd($body);
        }
        return View::make('user.profile', compact('user'));
    }

    /**
     * Récupère les informations de la session du joueur connecté
     */
    public function getSessionData()
    {
        $user_data = User::getSessionData();
        return Response::json($user_data);
    }

    /**
     * Récupère le userid
     */
    public function getUserId()
    {
        return Response::json(['user' => ['id' => Session::get('user.id')]]);
    }
}
