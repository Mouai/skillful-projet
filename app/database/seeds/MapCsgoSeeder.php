<?php

class MapCsgoSeeder extends Seeder {

    public function run()
    {
        DB::table('map_csgo')->delete();

        MapCsgo::create(array('name' => 'de_dust2'));
        MapCsgo::create(array('name' => 'de_nuke'));
        MapCsgo::create(array('name' => 'de_inferno'));
        MapCsgo::create(array('name' => 'de_mirage'));
        MapCsgo::create(array('name' => 'de_overpass'));
        MapCsgo::create(array('name' => 'de_cbble'));
        MapCsgo::create(array('name' => 'de_train'));

    }

}