<?php

class TeamSeeder extends Seeder {

    public function run()
    {
        DB::table('team')->delete();

        Team::create(array('name' => 'All Against Autority',
            'tag' => 'aAa',
            'game_id' => 1));

        Team::create(array('name' => 'Team LDLC',
            'tag' => 'LDLC',
            'game_id' => 1));

        Team::create(array('name' => 'Millenium',
            'tag' => 'Millenium',
            'game_id' => 2));
    }

}