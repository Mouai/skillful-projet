<?php

class UserSeeder extends Seeder {

    public function run()
    {
        DB::table('user')->delete();

        User::create(array('alias' => 'Mouai',
        'email' => 'steven@gmail.com',
        'password' => Hash::make('test')));

        User::create(array('alias' => 'Rikou',
            'email' => 'eric@gmail.com',
            'password' => Hash::make('test')));

        User::create(array('alias' => 'Noob',
            'email' => 'noob@gmail.com',
            'password' => Hash::make('test')));
    }

}