<?php

class LevelCsgoSeeder extends Seeder {

    public function run()
    {
        DB::table('level_csgo')->delete();

        LevelCsgo::create(array('name' => 'Low'));
        LevelCsgo::create(array('name' => 'Middle'));
        LevelCsgo::create(array('name' => 'High'));
        LevelCsgo::create(array('name' => 'Sub Top'));
        LevelCsgo::create(array('name' => 'Top'));

    }

}