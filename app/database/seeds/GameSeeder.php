<?php

class GameSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        DB::table('game')->delete();

        Game::create(array('title' => 'Counter-Strike Global Offensive', 'tag' => 'CS:GO'));
        Game::create(array('title' => 'League of Legends', 'tag' => 'LoL'));
        Game::create(array('title' => 'Heartstone', 'tag' => 'HS'));
    }

} 