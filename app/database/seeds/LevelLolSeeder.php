<?php

class LevelLolSeeder extends Seeder {

    public function run()
    {
        DB::table('level_lol')->delete();

        LevelLol::create(array('name' => 'Bronze'));
        LevelLol::create(array('name' => 'Argent'));
        LevelLol::create(array('name' => 'Or'));
        LevelLol::create(array('name' => 'Platine'));
        LevelLol::create(array('name' => 'Diamant'));

    }

}