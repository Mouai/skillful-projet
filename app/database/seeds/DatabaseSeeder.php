<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserSeeder');
        $this->call('GameSeeder');
		$this->call('TeamSeeder');
		$this->call('TeamUserSeeder');

		$this->call('LevelCsgoSeeder');
		$this->call('MapCsgoSeeder');
		$this->call('LevelLolSeeder');



		$this->call('MatchmakingSeeder');
	}

}
