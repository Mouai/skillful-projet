<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatchmakingLol extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchmaking_lol', function($table)
        {
            //$table->increments('id');
            $table->integer('matchmaking_id')->unsigned();
            $table->integer('level_id')->unsigned();
            $table->timestamps();
            $table->foreign('matchmaking_id')->references('id')->on('matchmaking');
            $table->foreign('level_id')->references('id')->on('level_lol');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matchmaking_lol', function($table){
            $table->dropForeign('matchmaking_id');
            $table->dropForeign('level_id');
        });
    }

}
