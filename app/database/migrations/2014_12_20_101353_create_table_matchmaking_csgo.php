<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatchmakingCsgo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchmaking_csgo', function($table)
        {
            //$table->increments('id');
            $table->increments('matchmaking_id')->unsigned();
            $table->integer('map_id')->unsigned();
            $table->integer('level_id')->unsigned();
            $table->boolean('server');
            $table->timestamps();

            $table->foreign('matchmaking_id')->references('id')->on('matchmaking');
            $table->foreign('map_id')->references('id')->on('map_csgo');
            $table->foreign('level_id')->references('id')->on('level_csgo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('matchmaking_csgo', function($table){
            $table->dropForeign('matchmaking_id');
            $table->dropForeign('map_id');
            $table->dropForeign('level_csgo');
        });
    }

}