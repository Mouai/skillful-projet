<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTeam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('name');
            $table->string('tag');
            $table->integer('game_id')->unsigned();
            $table->timestamps();

            $table->foreign('game_id')->references('id')->on('game');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('team', function($table){
            $table->dropForeign('game_id');
        });
	}

}
