<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatchmaking extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchmaking', function($table)
        {
            $table->increments('id');
            $table->integer('game_id')->unsigned();
            $table->integer('searcher_id')->unsigned();
            $table->integer('opponent_id')->unsigned();
            $table->float('reputation');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matchmaking', function($table){
            $table->dropForeign('game_id');
            $table->dropForeign('searcher_id');
            $table->dropForeign('opponent_id');
        });
    }

}
