var that;

function Client(user_id){
    //connexion au serveur node
    this.socket = io.connect('http://localhost:1337');
    this.btn_go = $('#btn_go');
    this.btn_search = $('#search');
    this.user_id = user_id;
    this.url_ajax_get_filter = "";
    that = this;
}

Client.prototype = {
    constructor: Client,
    /**
     * Initialisation du client
     */
    initialize: function(){
        this.socket.emit('authentification', {user_id: this.user_id});
        //this.socket.on('', this.onStartSearch);
        this.socket.on('matchFound', this.onMatchFound);
        this.socket.on('newMessage', this.onReceiveMessage);
        $('#btn_go').click(function(){
            that.generateParametersHTML();
        });


        $(document).on('click','.start_search',function(){
            //var jsonArr = {team_id: 1};
            var jsonArr = {};
            jsonArr.team_id = $('#team_id').val();
            //alert(jsonArr.team_id);
            that.socket.emit('loadparameters', { jsonArr: jsonArr });
        });
    },
    /**
     * AJAX générant les parametres HTML
     */
    generateParametersHTML: function(){
        var team_id = $('#team_id').val();
        $.ajax({
            url: this.url_ajax_get_filter,
            type: "POST",
            dataType: "HTML",
            data : $('form.select_team').serialize(),
            //data: {'team_id': team_id},
            success: function (data)
            {
                $('#filters').html(data);
                //alert(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("ajax en erreur");
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    },
    /**
     * Début de la recherche de match
     */
    onStartSearch: function(){
        console.log('début de la recherche');
    },
    onMatchFound: function(data){
        console.log('match trouvé contre : ' +data);

        var socket = this;
        $('#btn_send').click(function(){
            var msg = $("#msg").val();
            $("#msg").val('');
            console.log('msg :'+msg);
            socket.emit('sendMessage', { msg: msg });
            $('.content-messages').append('<br/><div style="float:right;">Me : '+msg+'</div>');
            $('.content-messages').animate({
                scrollTop: $('.content-messages')[0].scrollHeight
            }, 'fast');

        });

        //this.on('newMessage', socket.onReceiveMessage);

    },
    onReceiveMessage: function(data){
        console.log(data.msg);
        $('.content-messages').append('<br/><div style="float:left;">'+data.author+' : '+data.msg+'</div>')
        $('.content-messages').animate({
            scrollTop: $('.content-messages')[0].scrollHeight
        }, 'fast');
    }
}