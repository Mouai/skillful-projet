var Match = require('./Match'),
    Player = require('./Player');

var memcache = require('memcache'),
    fs = require('fs'),
    co = require("./cookie.js"),
    io = require('socket.io'),
    express = require('express'),
    redis = require('redis'),
    http = require('http'),
    mysql = require('mysql');


function Server (port) {
    this.app = express();
    this.server = http.createServer(this.app);

    this.io = io.listen(this.server);
    this.users = [];
    this.matchs = [];
    this.FPS = 1;
    this.milliWait = 1000/this.FPS;
    this.everysecond = 0;
    this.second = 0;

    this.redisClient = null;

    this.api = "http://localhost/skillful/public/";

    this.prev_tick = new Date().getTime();
    this.ms = this.milliWait;

    this.mysql_connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '',
        database: 'skillful'
    });

    this.server.listen(1337);

    this.initialize();
}

Server.prototype = {

    constructor: Server,
    /**
     * Initialisation du serveur
     */
    initialize: function (){
        console.log('Server Started');
        //this.socket = this.io.listen(this.port);
        //this.socket.set('log level', 1); // reduce logging



        //Boucle de rafraichissement du serveur
        this.serverLoop();
        //this.loadData();
        this.io.sockets.on("connection", this.onSocketConnection);
    },
    /**
     * Boucle du serveur
     */
    serverLoop: function(){

        if(this.everysecond == this.FPS){ // 1s
            //console.log(this.second++);
            this.doSearch();
            this.prepareMatch();
            this.everysecond = 0;
        }

        /*if(this.everysecond % (this.FPS/10) == 0){ // 100ms
         //this.socket.emit('sendToAllXY', players);
         }*/

        this.everysecond++;
        //Interval entre les deux tick
        this.tick = new Date().getTime();
        this.lostTime = this.tick - this.prev_tick;

        if(this.lostTime > this.milliWait)
            this.ms = (this.milliWait *2) - this.lostTime;
        else
            this.ms = this.milliWait - (this.milliWait - this.lostTime);

        if(this.ms < 0)
            this.ms = 0;

        this.prev_tick = new Date().getTime();
        setTimeout(this.serverLoop.bind(this), this.ms);
    },
    /**
     * Socket de connection d'un joueur
     * @param socket
     */
    onSocketConnection: function(socket){
        //console.log('user connected '+ socket.id);

        var cookie = socket.handshake.headers.cookie,
            sessionID = cookie.substr('laravel_session='.length, cookie.length);
        console.log(sessionID);

        //to decode payload use Buffer
        var serialized_session = new Buffer(payload, 'base64').toString();
        //to unserialize php session use PHPUnserialize node package
        var session = PHPUnserialize.unserializeSession(serialized_session);

        //Redis Client
        this.redisClient = redis.createClient();
        this.redisClient.subscribe('users.update');

        //Ecouteurs d'evenements
        socket.on("authentification", server.onAuthentification);
        socket.on("loadparameters", server.onLoadParameters);
        socket.on("sendMessage", server.onReceiveMessage);
        socket.on("disconnect", server.onClientDisconnect);
    },
    /**
     * Socket de déconnexion d'un user
     * @param socket
     */
    onClientDisconnect: function(socket){
        //console.log("undefined : "+socket.id);
        this.redisClient.quit();
        console.log(server.users[this.id].data.alias+" s'est deconnecté");
        delete server.users[socket.id];

    },
    /**
     * Socket d'authentification d'un utilisateur
     * @param data
     */
    onAuthentification: function(data){
        //console.log('id: ' + data.user_id+ " socket_id "+ this.id);
        //server.connection.connect();
        var query = server.mysql_connection.query("SELECT * FROM user WHERE id = "+data.user_id);
        query.on('error', function(err) {
            throw err;
        });

        var player_socket = this;

        query.on('result', function(data) {

            //console.log(row);
            var p = new Player(data, player_socket);

            server.users[player_socket.id] = p;
            console.log(server.users[player_socket.id].data.alias + " s'est connecté");
        });
        //server.mysql_connection.end();
    },

    /**
     * Charge les parametres de la partie du joueur
     */
    onLoadParameters: function(data){
        var team_id = data.jsonArr.team_id;
        server.loadDataTeamUser(this.id, team_id);
    },
    /**
     * Enregistre les paramètres choisit de l'utilisateur
     * @param json_object
     */
    initUser: function(socket_id, userDataJson){
        var userData = JSON.parse(userDataJson);
        console.log(this.users[socket_id].data.alias+" lance une recherche, team : "+userData.name);
        this.users[socket_id].team = userData;
        this.users[socket_id].ready = false;
        this.users[socket_id].search = true;
        this.users[socket_id].inmatch = false;
    },
    /**
     * Effectue la recherche de match entre les teams
     */
    doSearch: function(){
        //for(var i = 0; i < this.users)
        for( var key1 in this.users){
            var user1 = this.users[key1];

            for( var key2 in this.users){
                var user2 = this.users[key2];
                if(user1.data.id != user2.data.id  && user1.search && user2.search){
                    console.log('deux team correspondent'+user2.team.name+" & "+user1.team["name"]);
                    user1.search = false;
                    user2.search = false;
                    this.matchs.push(new Match(user1, user2));
                    user1.socket.emit('matchFound', user2.team.name);
                    user2.socket.emit('matchFound', user1.team.name);
                }
            }
        }
    },
    /**
     * Préparation du match
     */
    prepareMatch: function(){
        for(var key in this.matchs){
            if(this.matchs[key].user1){

            }


        }
    },
    /**
     *Chargement des informations de la team d'un utilisateur
     */
    loadDataTeamUser: function(socket_id, team_id){
        var url = server.api + "team/getTeamById/"+team_id;

        http.get(url, function(res) {
            var responseString = '';

            res.on('data', function(data) {
                responseString += data;
            });

            res.on('end', function() {
                var userData = JSON.parse(JSON.stringify(responseString));
                //console.log(userData);
                server.initUser(socket_id, userData);
            });

        }).on('error', function(e) {
            console.log("Got error: ", e);
        });
    },

    loadData: function(req){
        this.app.get('/', function (req, res) {
            res.sendfile(__dirname + '/public');
            var cookieManager = new co.cookie(req.headers.cookie);

            var client = new memcache.Client(11211, "localhost");
            client.connect();

            var user = client.get("sessions/"+cookieManager.get("sec_session_id"), function(error, result){
                var session = JSON.parse(result);
                user = JSON.parse(session.name);
                user = user.username;
                console.log('here '+user.username);
                //storeUsername(user);
            });

        });

    },
    onReceiveMessage: function(data){
        console.log(data.msg);
        var author = server.users[this.id].data.alias;
        this.broadcast.emit('newMessage', {msg: data.msg, author: author});
        //server.users[this.socket.id]
    }
}

var server = new Server(1337);

