function Player(p_data, p_socket)
{
    this.data = p_data;
    this.socket = p_socket;
}

Player.prototype =
{
    constructor: Player
}

module.exports = Player;

